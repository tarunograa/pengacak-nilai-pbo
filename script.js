var cards = document.querySelectorAll('.card');
var items1 = [70,71,72,73,74,75,76,77,78,79,80];
var items2 = [76,77,78,79,80,80,81,82,83,84,85,86,87,88,89,90];
var items3 = [85,86,87,88,89,90,91,92,93,94,95];
var buttonOpen = document.getElementsByName("open")[0];
var buttonClose = document.getElementsByName("close")[0];
var buttonRandom = document.getElementsByName("random")[0];
var buttonShuffle = document.getElementsByName("shuffle")[0];

newRandom()

function getState(cards){
  var states = []
  cards.forEach((card)=>{
    states.push(card.className);
  })
  return states;
}

function newRandom(){
  var shuffled = generateScores();
  cards.forEach((card, index)=>{
  card.firstElementChild.innerHTML = shuffled[index];
})
}

var shuffled

function generateScores(){
  var scores = []
  scores[0] = items1[Math.floor(Math.random()*items1.length)];
  scores[1] = items1[Math.floor(Math.random()*items1.length)];
  scores[2] = items2[Math.floor(Math.random()*items2.length)];
  scores[3] = items2[Math.floor(Math.random()*items2.length)];
  scores[4] = items3[Math.floor(Math.random()*items3.length)];
  shuffled = scores
  .map(value => ({ value, sort: Math.random() }))
  .sort((a, b) => a.sort - b.sort)
  .map(({ value }) => value)

  return shuffled
}

cards.forEach((card)=>{
  card.addEventListener( 'click', function() {
    card.classList.toggle('is-flipped');
    var states = getState(cards)
    if(!states.includes('card is-flipped')){
      setTimeout(function() {
        newRandom()
      }, 300);
    }
  });
});

buttonOpen.addEventListener('click',function(){
  cards.forEach((card)=>{
    card.className = 'card is-flipped'
  })
});

buttonClose.addEventListener('click', function(){
  cards.forEach((card)=>{
    card.className = 'card'
  })
})

buttonRandom.addEventListener('click', function(){
  newRandom();
})

buttonShuffle.addEventListener('click', function(){
  shuffled = shuffled
  .map(value => ({ value, sort: Math.random() }))
  .sort((a, b) => a.sort - b.sort)
  .map(({ value }) => value)
  cards.forEach((card, index)=>{
    card.firstElementChild.innerHTML = shuffled[index];
  })
  
})

